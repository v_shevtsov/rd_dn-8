applyAll(sum, 1, 2, 3, 4, 5, 6, 7);
applyAll(mul, 1, 2, 3, 4);

// ES6
function sum(rest) {
    return rest.reduce((a, b) => a + b);
}

function mul(rest) {
    return rest.reduce((a, b) => a * b);
}

function applyAll(func, ...rest) {
    console.log(func(rest));
}

// !ES 6

// function argumentsToArray(args) {
//     return [].slice.call(args);
// }
//
// function sum() {
//     var argumentsArray = argumentsToArray(arguments);
//
//     return argumentsArray.reduce(function (a, b) {
//         return a + b;
//     })
// }
//
// function mul() {
//     var argumentsArray = argumentsToArray(arguments);
//
//     return argumentsArray.reduce(function (a, b) {
//             return a * b;
//         }
//     )
// }
//
// function applyAll(func) {
//     var argumentsArray = argumentsToArray(arguments);
//
//     var funcArguments = argumentsArray.filter(function (item, index) {
//         return index !== 0;
//     });
//
//     console.log(func.apply(null, funcArguments));
// }