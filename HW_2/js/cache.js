function cache(func) {
    const cacheObj = {};

    return function () {
        const argumentsString = [...arguments].join();
        let result;

        if (typeof cacheObj[argumentsString] !== 'undefined') {
            return `cache: ${cacheObj[argumentsString]}`;
        }

        result = func(...arguments);
        cacheObj[argumentsString] = result;

        return result;
    }
}

let complexFunction = function (arg1, arg2) {
    return arg1 + arg2;
}

let cachedFunction = cache(complexFunction);

console.log(cachedFunction('foo', 'bar'));

console.log(cachedFunction('foo', 'bar'));

console.log(cachedFunction('foo', 'baz'));